---
layout: page
title: Czemu Blog?
subtitle: Czemu blog a nie coś innego?
---

Od dłuższego czasu zabierałem się na stworzenie  tego bloga. Odkładałem to z powodów ważnych i ważniejszych jak i tych błahych. W końcu udało mi się wsiąść za siebie i o to jest mój pierwszy w życiu blog. Niektórzy twierdzą że mam za dużo czasu i zabrałem się za głupoty. Ja podchodzę do tego inaczej. Jest to repozytorium wiedzy, gdzie będę umieszczał rozwiązania, które pomagają mi codziennych trudach bycia adminem systemów wszelakich. Już kilka razy złapałem się na tym że zapominam  jak coś kiedyś zrobiłem i muszę wymyślać koło na nowo. Mógł bym notatki trzymać tylko dla siebie, na dysku czy w chmurze, ale po co jak można się nimi podzielić co pozwoli komuś zaoszczędzić czas. Więc wydaje się że prowadzenia bloga ma same plusy. Sam w poszukiwaniu inspiracji albo rozwiązań przeglądam blogi i portale o tematyce około Sysopowej. Teraz przyszedł czas żeby wiedzę zdobytą w konsolowym boju oddać z nawiązką  innym potrzebującym. Będę starał się pisać w taki sposób żeby nawet laik
