FROM ruby:2.4-slim

MAINTAINER Łukasz Żułnowski "lzulnowski@gmail.com"

RUN apt-get update && apt-get install -y -q \
  build-essential \
  wget \
  curl

RUN gem install jekyll bundler

RUN mkdir -p /app
WORKDIR /app

EXPOSE 4000

CMD bundle install && bundle exec jekyll serve -H 0.0.0.0
